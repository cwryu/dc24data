#!/usr/bin/env python3

from csv import DictWriter
from datetime import date, datetime
from time import mktime

import stripe


PAYMENT_COLUMNS = (
    'Description',
    'Created (UTC)',
    'Amount',
    'Amount Refunded',
    'Currency',
    'Converted Amount',
    'Converted Amount Refunded',
    'Fee',
    'Converted Currency',
    'Status',
    'Captured',
    'reference_number (metadata)',
    'breakdown_accommodation (metadata)',
    'breakdown_meals (metadata)',
    'breakdown_registration (metadata)',
)

PAYOUT_COLUMNS = (
    'Amount',
    'Created (UTC)',
    'Currency',
    'Destination Name',
)

TOPUP_COLUMNS = (
    'Amount',
    'Created (UTC)',
    'Currency',
    'Source Name',
)


def login():
    with open('.stripe-api-key') as f:
        stripe.api_key = f.read().strip()


def fmt_currency(number):
    """
    Convert a cent-denominated integer to a comma-decimal-separated string
    """
    return f'{number/100:.2f}'.replace('.', ',')


def fmt_timestamp(timestamp):
    """
    Convert a unix timestamp to YYYY-MM-DD HH:MM
    """
    return (datetime.utcfromtimestamp(timestamp)
            .isoformat().replace('T', ' ')[:16])


def list_payments():
    """
    Equivalent to exporting CSV from the All Payments view in the Dashboard
    """
    since = int(mktime(date.today().replace(day=1, month=1).timetuple()))
    transactions = stripe.BalanceTransaction.list(
        created={'gte': since},
        expand=['data.source.payment_intent'],
    )
    for txn in transactions.auto_paging_iter():
        if txn.type != 'charge':
            continue
        charge = txn.source
        payment = charge.payment_intent
        refunded = (charge.amount_refunded or 0)
        yield {
            'Description': txn.description,
            'Created (UTC)': fmt_timestamp(charge.created),
            'Amount': fmt_currency(payment.amount),
            'Amount Refunded': fmt_currency(refunded),
            'Currency': payment.currency,
            'Converted Amount': fmt_currency(txn.amount),
            'Converted Amount Refunded':
                fmt_currency(refunded * (txn.exchange_rate or 1)),
            'Fee': fmt_currency(txn.fee),
            'Converted Currency': txn.currency,
            'Status': {
                # Expand this when we come across more types
                'succeeded': 'Paid',
            }[payment.status],
            'Captured': str(charge.captured).lower(),
            'reference_number (metadata)': payment.metadata.reference_number,
            'breakdown_accommodation (metadata)':
                payment.metadata.get('breakdown_accommodation', 0),
            'breakdown_meals (metadata)':
                payment.metadata.get('breakdown_meals', 0),
            'breakdown_registration (metadata)':
                payment.metadata.get('breakdown_registration', 0),
        }


def list_payouts():
    """
    Equivalent to exporting CSV from the All Payments view in the Dashboard
    """
    since = int(mktime(date.today().replace(day=1, month=1).timetuple()))
    payouts = stripe.Payout.list(
        created={'gte': since},
        expand=['data.destination'],
    )
    for payout in payouts.auto_paging_iter():
        if payout.status != 'paid':
            continue
        destination = payout.destination
        yield {
            'Amount': fmt_currency(payout.amount),
            'Created (UTC)': fmt_timestamp(payout.created),
            'Currency': payout.currency,
            'Destination Name': destination.bank_name,
        }


def list_topups():
    """
    Equivalent to exporting CSV from the All TopUps view in the Dashboard
    """
    since = int(mktime(date.today().replace(day=1, month=1).timetuple()))
    topups = stripe.Topup.list(
        created={'gte': since},
        expand=['data.source'],
    )
    for topup in topups.auto_paging_iter():
        if topup.status != 'succeeded':
            continue
        source = topup.source
        yield {
            'Amount': fmt_currency(topup.amount),
            'Created (UTC)': fmt_timestamp(topup.created),
            'Currency': topup.currency,
            'Source Name': source.get(source.type).bank_name,
        }


def main():
    login()
    with open('stripe/payments.csv', 'w') as f:
        w = DictWriter(f, fieldnames=PAYMENT_COLUMNS)
        w.writeheader()
        for p in list_payments():
            w.writerow(p)
    with open('stripe/payouts.csv', 'w') as f:
        w = DictWriter(f, fieldnames=PAYOUT_COLUMNS)
        w.writeheader()
        for p in list_payouts():
            w.writerow(p)
    with open('stripe/topups.csv', 'w') as f:
        w = DictWriter(f, fieldnames=TOPUP_COLUMNS)
        w.writeheader()
        for t in list_topups():
            w.writerow(t)


if __name__ == '__main__':
    main()
